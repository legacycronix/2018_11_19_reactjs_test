
export interface Owner {
  avatar_url: string;
  html_url: string;
  id: number;
  login: string;
}
