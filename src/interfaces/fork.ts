import { Owner } from './owner';

export interface Fork {
  id: number;
  stargazers_count: number;
  html_url: string;
  owner: Owner;
  name: string;
}
