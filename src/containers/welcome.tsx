import { Grid, Paper, Theme, Typography, withStyles, WithStyles } from '@material-ui/core';
import { History } from 'history';
import React from 'react';
import { SearchComponent } from '../components/search';

const styles = (theme: Theme) => ({
  main: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '15%',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column' as any,
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  header: {
    textAlign: 'center' as any,
  },
});

interface Props extends WithStyles<typeof styles> {
  history: History;
}
@(withStyles as any)(styles)
export class WelcomeContainer extends React.Component<Props> {
  submit = (value: string) => {
    this.props.history.push({
      pathname: '/search',
      search: `?repository=${value}&page=1`,
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <Grid container={true}>
          <Grid item={true} xs={12} lg={12}>
            <Typography component="h4" variant="h4">
              <h4 className={classes.header}>Welcome to Github Fork!</h4>
            </Typography>
          </Grid>
          <Grid item={true} xs={12} lg={12}>
            {/* <Paper> */}
            <SearchComponent onSubmit={this.submit} />
            {/* </Paper> */}
          </Grid>
        </Grid>
      </main>
    );
  }
}
