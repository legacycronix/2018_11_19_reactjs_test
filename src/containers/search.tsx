import {
  Avatar, Button, CircularProgress, Grid, Table,
  TableBody, TableCell, TableFooter, TableHead, TablePagination,
  TableRow, Theme, withStyles, WithStyles,
} from '@material-ui/core';
import { ArrowBack, ArrowBackIos, ArrowForward, NavigateBefore } from '@material-ui/icons';
import { History } from 'history';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { getForksRequest } from '../actions/forks';
import { ErrorComponent } from '../components/error';
import { ForkComponent } from '../components/fork';
import { Fork } from '../interfaces/fork';
import { RootState } from '../store/reducer';

const styles = (theme: Theme) => ({
  backButton: {
    position: 'fixed' as any,
    left: '30px',
    top: '50px',
  },
  nextPageButton: {
    position: 'fixed' as any,
    left: '30px',
    top: '150px',
  },
  prevPageButton: {
    position: 'fixed' as any,
    left: '30px',
    top: '226px',
  },

  spinnerWrapper: {
    width: 400,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '15%',
    display: 'flex',
    alignItems: 'center' as any,
    justifyContent: 'center' as any,
  },
});

interface Props extends WithStyles<typeof styles> {
  history: History;
  location: Location;
  actions: {
    getForksRequest: typeof getForksRequest;
  };
  items: Fork[];
  error: string | null;
  loading: boolean;
}
interface State {

}

const mapStateToProps: any = (state: RootState) => ({
  items: state.forks.items,
  error: state.forks.error,
  loading: state.forks.loading,
});
const mapDispatchToProps: any = (dispatch: Dispatch<any>) => {
  return ({
    actions: bindActionCreators({ getForksRequest }, dispatch),
  });
};

@(withStyles as any)(styles)
@(connect as any)(mapStateToProps, mapDispatchToProps)
export class SearchContainer extends React.Component<Props, State> {
  componentDidMount() {
    this.getForks();
  }

  getForks = () => {
    const params = new URLSearchParams(this.props.location.search);
    const repoName = params.get('repository');
    const page = params.get('page') || '1';
    if (repoName) {
      this.props.actions.getForksRequest(repoName, +page);
    }
  }

  changePage = (n: number) => () => {
    const params = new URLSearchParams(this.props.location.search);
    const repoName = params.get('repository');
    const page = params.get('page');
    if (!repoName || !page || (+page === 1 && n < 0)) {
      return;
    }
    this.props.history.push({
      pathname: '/search',
      search: `?repository=${repoName}&page=${+page + n}`,
    });
  }
  componentWillReceiveProps(nextProps: Props) {
    if (this.props.location.search !== nextProps.location.search) {
      this.getForks();

    }
  }

  goBack = () => {
    this.props.history.go(-1);
  }

  render() {
    const { classes } = this.props;

    return (<>
      <Grid container={true} justify="center">
        <Button
          variant="fab"
          color="secondary"
          aria-label="Back"
          className={classes.backButton}
          onClick={this.goBack}
          title="Go back"
        >
          <NavigateBefore />
        </Button>
        <Button
          variant="fab"
          color="primary"
          aria-label="Back"
          className={classes.prevPageButton}
          onClick={this.changePage(-1)}
          title="Previous page"
        >
          <ArrowBack />
        </Button>
        <Button
          variant="fab"
          color="primary"
          aria-label="Back"
          className={classes.nextPageButton}
          onClick={this.changePage(1)}
          title="Next page"
        >
          <ArrowForward />
        </Button>
        <Grid item={true} lg={8} xl={6} md={8} sm={12}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Repository name</TableCell>
                <TableCell>Owner</TableCell>
                <TableCell>Stars count</TableCell>
                <TableCell>Link</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.props.items.map(fork => <ForkComponent key={fork.id} fork={fork} />)}
            </TableBody>
            <TableFooter>
              {/* <TableRow>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActionsWrapped}
                />
              </TableRow> */}
            </TableFooter>
          </Table>
          <Grid item={true} lg={12} sm={12}>
            {this.props.loading &&
              <div className={classes.spinnerWrapper}>
                <CircularProgress />
              </div>}
            {this.props.error && <ErrorComponent message={this.props.error as string} />}
          </Grid>
        </Grid>
      </Grid>
    </>);
  }
}
