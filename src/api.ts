import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'https://api.github.com',
  timeout: 1000,
  headers: { Accept: 'application/vnd.github.v3+json' },
});

const getForks = (query: string, page: number) => {
  const [owner, repo] = query.split('/');
  return axiosInstance.get(`/repos/${owner}/${repo}/forks?page=${page}`);
};

export const api = {
  getForks,
};
