import { Avatar } from '@material-ui/core';
import React from 'react';
import { Owner } from '../interfaces/owner';

interface Props {
  owner: Owner;
}
// tslint:disable-next-line:variable-name
export const OwnerComponent = ({ owner }: Props) => {
  return (<div style={{ display: 'flex' }} >
    <Avatar sizes="30" alt={owner.login} src={owner.avatar_url} />
    <p>&nbsp;{owner.login}</p>
  </div>);
};
