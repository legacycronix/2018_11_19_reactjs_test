import { Paper, Theme, Typography, withStyles, WithStyles } from '@material-ui/core';
import React from 'react';

const styles = (theme: Theme) => ({
  paper: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '15%',
    },
    textAlign: 'center' as any,
  },
});

interface Props extends WithStyles<typeof styles> {
  message: string;
}

// tslint:disable-next-line:variable-name
export const ErrorComponent = withStyles(styles)(
  ({ message, classes }: Props) => {
    return (
      <div className={classes.paper}>
        <Typography variant="h5" gutterBottom={true}>
          {message}
        </Typography>
      </div>
    );
  },
);
