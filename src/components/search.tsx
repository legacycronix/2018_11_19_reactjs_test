import { TextField } from '@material-ui/core';
import React from 'react';

interface Props {
  onSubmit: Function;
}
interface State {
  value: string;
  isValid: boolean;
}
export class SearchComponent extends React.Component<Props, State> {
  state = {
    value: '',
    isValid: true,
  };
  onChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    const value = ev.target.value;
    this.setState({ value });
  }

  validate = (): boolean => {
    return Boolean(this.state.value.match(
      new RegExp('[\\w-]{3,}/[\\w-]{3,}'),
    ));
  }

  onKeyPress = (ev: any) => {
    if (ev.key === 'Enter') {
      ev.preventDefault();
      const isValid = this.validate();
      this.setState({ ...this.state, isValid });
      if (isValid) {
        this.props.onSubmit(this.state.value);
      }
    }
  }

  render() {
    const helperText = this.state.isValid
      ? 'Repository name format: :owner/:repoName'
      : 'Incorrect repository name format. Use :owner/repoName';
    return (
      <>
        <TextField
          error={!this.state.isValid}
          label="Type a repo name and press enter"
          margin="normal"
          variant="outlined"
          onChange={this.onChange}
          value={this.state.value}
          onKeyPress={this.onKeyPress}
          helperText={helperText}
          fullWidth={true}
        />
      </>
    );
  }
}
