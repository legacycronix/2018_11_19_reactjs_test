import { TableCell, TableRow } from '@material-ui/core';
import { Star } from '@material-ui/icons';
import React from 'react';
import { Fork } from '../interfaces/fork';
import { OwnerComponent } from './owner';

interface Props {
  fork: Fork;
}

// tslint:disable-next-line:variable-name
export const ForkComponent = ({ fork }: Props) => {
  return (
      <TableRow key={fork.id}>
        <TableCell component="th" scope="row">
          {fork.name}
        </TableCell>
        <TableCell component="th" scope="row">
          <OwnerComponent owner={fork.owner} />
        </TableCell>
        <TableCell component="th" scope="row">
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Star />&nbsp;{fork.stargazers_count}
          </div>
        </TableCell>
        <TableCell component="th" scope="row">
          <a href={fork.html_url}>{fork.html_url}</a>
        </TableCell>
      </TableRow>
  );
};
