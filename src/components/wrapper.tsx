import { Theme, withStyles, WithStyles } from '@material-ui/core';
import React from 'react';

const styles = (theme: Theme) => ({
  wrapper: {
    backgroundColor: theme.palette.background.default,
  },
});

interface Props extends WithStyles<typeof styles> {
}

@(withStyles as any)(styles)
export class WrapperComponent extends React.Component<Props> {
  render() {
    return (
      <div className={this.props.classes.wrapper}>{this.props.children}</div>
    );
  }
}
