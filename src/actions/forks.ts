import { api } from '../api';

export const GET_FORKS_REQUEST = 'GET_FORKS_REQUEST';
export const GET_FORKS_SUCCESS = 'GET_FORKS_SUCCESS';
export const GET_FORKS_FAIL = 'GET_FORKS_FAIL';

export const getForksRequest = (query: string, page: number) => {
  return async (dispatch: any) => {
    dispatch({ type: GET_FORKS_REQUEST, payload: { query, page } });
    try {
      const result = await api.getForks(query, page);
      dispatch({ type: GET_FORKS_SUCCESS, payload: { forks: result.data } });
    } catch (e) {
      let error = 'Unknown Error';
      if (e.code === 'ECONNABORTED') {
        error = 'Connection error';
      }
      dispatch({ type: GET_FORKS_FAIL, payload: { error } });
    }
  };
};
