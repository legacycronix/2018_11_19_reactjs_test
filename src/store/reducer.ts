import { combineReducers } from 'redux';
import { forksReducer as forks, ForksState } from '../reducers/forks';

export interface RootState {
  forks: ForksState;
}

export const rootReducer = combineReducers<RootState>({
  forks,
});
