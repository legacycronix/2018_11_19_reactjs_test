import { applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import freezeMiddleware from 'redux-freeze';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { rootReducer, RootState } from './reducer';

const loggerMiddleware = createLogger({
  collapsed: true,
  diff: true,
});
export function configureStore(initialState?: any): Store {
  const middlewares: any[] = [
    thunkMiddleware,
  ];
  if (process.env.NODE_ENV !== 'production') {
    middlewares.push(freezeMiddleware, loggerMiddleware);
  }

  let middleware = applyMiddleware(...middlewares);

  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(
    rootReducer,
    initialState,
    middleware,
  );

  return store;
}
