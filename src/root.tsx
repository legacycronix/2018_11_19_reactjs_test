import CssBaseline from '@material-ui/core/CssBaseline';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createBrowserHistory } from 'history';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Router, Switch } from 'react-router-dom';
import { Store } from 'redux';
import { WrapperComponent } from './components/wrapper';
import { SearchContainer } from './containers/search';
import { WelcomeContainer } from './containers/welcome';
import { theme } from './theme';

export const history = createBrowserHistory();

interface Props {
  store: Store;
}
// tslint:disable-next-line:variable-name
export const Root = ({ store }: Props) => {
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <WrapperComponent classes={{} as any}>
          <Router history={history}>
            <Switch>
              <Route path="/" exact={true} component={WelcomeContainer} />
              <Route path="/search" component={SearchContainer} />
            </Switch>
          </Router>
        </WrapperComponent>
      </MuiThemeProvider>
    </Provider>
  );
};
