import { GET_FORKS_FAIL, GET_FORKS_REQUEST, GET_FORKS_SUCCESS } from '../actions/forks';
import { Fork } from '../interfaces/fork';

export interface ForksState {
  items: Fork[];
  error: string | null;
  loading: boolean;
}

const initialState = {
  items: [],
  error: null,
  loading: false,
};

export const forksReducer = (state: ForksState = initialState, action: any): ForksState => {
  switch (action.type) {
    case GET_FORKS_REQUEST:
      return { ...state, items: [], loading: true, error: null };
    case GET_FORKS_SUCCESS:
      return { ...state, items: action.payload.forks, loading: false, error: null };
    case GET_FORKS_FAIL:
      return { ...state, error: action.payload.error, loading: false };
    default:
      return state;
  }
};
