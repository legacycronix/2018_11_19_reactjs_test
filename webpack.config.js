const Webpack = require('webpack');
const Path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const outPath = Path.join(__dirname, './dist');
const sourcePath = Path.join(__dirname, './src');
const assetsPath = Path.join(__dirname, './assets');
const isProduction = process.env.NODE_ENV === 'production';

const loaders = [
  {
    test: /\.tsx?$/,
    use: [
      {
        loader: 'ts-loader'
      }
    ]
  },
  {
    test: /\.(png|jpg|gif|svg|jpeg|ico)$/,
    include: [sourcePath, assetsPath],
    use: [{
      loader: 'file-loader',
      options: {
        name: './assets/[name].[hash].[ext]'
      }
    }]
  }
];

let plugins = [
  new HtmlWebpackPlugin({
    template: './index.html',
    favicon: '../assets/favicon.ico',
    hash: true
  }),
];

if (!isProduction) {
  plugins = [
    ...plugins,
    new Webpack.NamedModulesPlugin(),
    new Webpack.HotModuleReplacementPlugin()
  ];
} 

const config = {
  stats: {
    children: false
  },
  target: 'web',
  // The base directory, an absolute path,
  // for resolving entry points and loaders from configuration.
  context: sourcePath,
  entry: {
    main: ['./index.tsx']
  },
  devtool: isProduction ? 'none' : 'source-map',
  output: {
    // the target directory for all output files
    // must be an absolute path (use the Node.js path module)
    path: outPath,
    // the url to the output directory resolved relative to the HTML page
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: 'lazy_[name].[chunkhash].js'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    modules: ['node_modules', sourcePath]
  },
  module: {
    rules: loaders
  },
  devServer: {
    historyApiFallback: true,
    disableHostCheck: true,
    compress: true,
    host: '127.0.0.1',
    port: process.env.PORT || 3000,
    hot: !isProduction,
    stats: {
      children: false
    },
    headers: {
      'Access-Control-Allow-Origin': '*'
    } 
  },
  plugins
};

module.exports = config;